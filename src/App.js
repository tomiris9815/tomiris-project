import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";
import LoginLayout from "./components/layouts/login-layout/LoginLayout";
import MainLayout from "./components/layouts/main-layout/MainLayout";

export default class App extends Component {
  render() {
    return (
      <Switch>
        <Route path="/auth" component={LoginLayout} />
        <Route path="/" component={MainLayout} />
      </Switch>
    );
  }
}

