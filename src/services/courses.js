import api from "./api";

export function getCourses() {
  return api.get("/users");
}

export function createUser(data) {
  return api.post("/user", data);
}
