import React from "react";
import { NavLink } from "react-router-dom";

import "./Header.scss";

export default function Header() {
  return (
    <div className="Header">
     
      <NavLink className="Header__link" to="/users">
        Мои курсы
      </NavLink>
      <NavLink className="Header__link" to="/posts">
        Расписание
      </NavLink>
      <NavLink exact className="Header__link" to="/">
        Профиль
      </NavLink>
    </div>
  );
}
