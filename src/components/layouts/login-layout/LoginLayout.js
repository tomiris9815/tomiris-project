import React, { Component } from "react";

import "./LoginLayout.scss";

export default class LoginLayout extends Component {
  state = {
    login: "",
    password: ""
  };

  componentDidMount() {
    const token = localStorage.getItem("access_token");
    if (token) {
      this.props.history.push("/");
    }
  }

  onInputChange = (event, name) => {
    const newValue = {};
    newValue[name] = event.target.value;
    this.setState({ ...newValue });
  };

  onSubmit = event => {
    event.preventDefault();
    const { login, password } = this.state;
    localStorage.setItem("access_token", `${login}:${password}`);
    this.props.history.push("/");
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit} className="Login__form">
          <b className="logo">EDUCATION.KZ </b>
          <input
            className="Login__input"
            placeholder="Login"
            onChange={event => this.onInputChange(event, "login")}
            type="text"
          />
          <input
            className="Login__input"
            placeholder="Password"
            onChange={event => this.onInputChange(event, "password")}
            type="password"
          />
          <button 
            className="Login__button"
            type="submit">Login</button>
        </form>
      </div>
    );
  }
}
