import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";

import Header from "../../shared/header/Header";
import Home from "../../pages/home/Home";
import Courses from "../../pages/courses/Courses";
import Schedules from "../../pages/schedules/Schedules";

export default class MainLayout extends Component {
  componentDidMount() {
    const token = localStorage.getItem("access_token");
    if (!token) {
      this.props.history.push("/auth");
    }
  }

  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route path="/users" component={Courses} />
          <Route path="/posts" component={Schedules} />
        </Switch>
      </div>
    );
  }
}
