import React, { Component } from "react";
import Settings from "../settings/Settings";

import { getCourses } from "../../../services/courses";

class Courses extends Component {
  state = {
    courses: []
  };

  componentDidMount() {
    getCourses()
      .then(response => {
        this.setState({ courses: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
         <Settings />
        Мои курсы
        {/* <button onClick={this.props.history.goBack}>Back</button> */}
        {this.state.courses.map(course => (
          <p key={course.id}>{course.name}</p>
        ))}
      </div>
    );
  }
}

export default Courses;
