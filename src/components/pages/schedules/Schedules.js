import React, { Component } from "react";

import { getSchedule } from "../../../services/schedule";

class Schedules extends Component {
  state = {
    schedules: []
  };

  componentDidMount() {
    getSchedule()
      .then(response => {
        this.setState({ schedules: response.data });
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div>
        Мое расписание
        {/* <button onClick={this.props.history.goBack}>Back</button> */}
        {this.state.schedules.map(schedule => (
          <p key={schedule.id}>{schedule.body}</p>
        ))}
      </div>
    );
  }
}

export default Schedules;