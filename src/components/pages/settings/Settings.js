import React, { Component } from "react";
import { withRouter } from "react-router-dom";

class Settings extends Component {
  handleClick = () => {
    //some actions ...
    this.props.history.push("/");
  };

  render() {
    return (
      <div>
        <button onClick={this.handleClick}>Главная</button>
      </div>
    );
  }
}

export default withRouter(Settings);
